**Drupal Test Notifier** is a user script for your browser that notifies you when an [automated test (a.k.a SimpleTest)](http://drupal.org/simpletest) has completed. This allows you to go about your other business without checking back on the tab running your tests every few seconds waiting for it to complete. It also saves you from forgetting that you had a test running while you go about your other business.

*NOTE:* This is not a Drupal Module and cannot be installed as such.

  
Details
-------
Drupal Test Notifier aims to solve the problem of long-running tests in Drupal 6 ([SimpleTest](http://drupal.org/project/simpletest)) and Drupal 7 (in core). It is common to run the test and come back to it every few seconds/minutes to see if it has finished running. This pattern is especially common while tests are being developed.

This user script (GreaseMonkey Script) is installed to your browser to notify you when a test has completed. It is not installed as a standard Drupal module.


Installation
------------
Install the dependencies:

  * [GreaseMonkey extension for Firefox](https://addons.mozilla.org/en/firefox/addon/748) or a GreaseMonkey-compatible browser.

Then

  * [Click here to install or update Drupal Test Notifier](http://drupalcode.org/viewvc/drupal/contributions/modules/test_notifier/test_notifier.user.js?view=co&.user.js)


Features
--------

  * Displays an alert box when the testing results page has been loaded


Upcoming Features
-----------------
The following features still need to be added

  * Display an alert if the /batch test loading page fails with an error and is unable to perform the redirect to the results page
  * Change the alert box to be similar to the "downloads complete" dialog in Firefox that is less intrusive

Please post features requests and bug reports to the [Issue Queue](http://drupal.org/project/issues/test_notifier).


Command Line Notifications
--------------------------
If you run your tests from the command line (terminal) using run-tests.sh ([details](http://drupal.org/node/645286)) then there is another way you can be notified when your tests complete. Install [growlnotify](http://growl.info/extras.php#growlnotify) and run your tests in a fashion similar to this:

  php scripts/run-tests.sh --url http://d7.localhost/ --color --verbose --class MyAwesomeTestCase; growlnotify --sticky -t 'Tests complete!' --message 'Check them out!'


Related Projects
----------------
Other Drupal user scripts:

  * [Dreditor](http://drupal.org/project/dreditor) - To make patch reviews on Drupal.org easier 


Development
-----------
This module was developed by [auzigog](http://drupal.org/user/542166) (Jeremy Blanchard) at [Activism Labs](http://activismlabs.org).