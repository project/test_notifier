// ==UserScript==
// @name          Drupal Test Notifier
// @namespace     http://drupal.org/project/simpletest_notifier
// @description   Notifies you when you're automated test in Drupal have finished running!
// @author        Jeremy Blanchard (auzigog)
// @version       0.1
// @include       http*://*/admin/config/development/testing/results/*
// @include       http*://*/admin/build/testing/results/*
// ==/UserScript==

alert('Test complete on '+window.location);